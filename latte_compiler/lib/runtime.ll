@newline = internal constant [4 x i8] c"%d\0A\00"
@err = internal constant [14 x i8] c"RUNTIME ERROR\00"

declare i32 @printf(i8*, ...) 
declare i32 @scanf(i8*, ...)
declare i32 @puts(i8*)
declare i8* @gets(i8*)
declare i8* @malloc(i32)
declare i32 @strlen(i8*)
declare i8* @strcpy(i8*, i8*)
declare i8* @strcat(i8*, i8*)
declare void @exit(i32)

define void @printInt(i32 %int) {
    %ptr0 = getelementptr inbounds [4 x i8], [4 x i8]* @newline, i32 0, i32 0
    call i32 (i8*, ...) @printf(i8* %ptr0, i32 %int) 
    ret void
}

define void @printString(i8* %newline) {
    call i32 @puts(i8* %newline)
    ret void
}

define i32 @readInt() {
    %al = alloca i32
    %r1 = getelementptr inbounds [4 x i8], [4 x i8]* @newline, i32 0, i32 0
    call i32 (i8*, ...) @scanf(i8* %r1, i32* %al)
    %r2 = load i32, i32* %al
    ret i32 %r2
}

define i8* @readString() {
    %r1 = call i8* @malloc(i32 256)
    %r2 = call i8* @gets(i8* %r1)
    ret i8* %r2
}

define void @error() {
    %r1 = getelementptr inbounds [14 x i8], [14 x i8]* @err, i32 0, i32 0
    call void @printString(i8* %r1)
    call void @exit(i32 1)
    ret void
}

define i8* @concat_(i8* %str1, i8* %str2) {
    %r1 = call i32 @strlen(i8* %str1)
    %r2 = call i32 @strlen(i8* %str2)
    %r3 = add i32 %r1, 1
    %r4 = add i32 %r3, %r2
    %r5 = call i8* @malloc(i32 %r4)
    %r6 = call i8* @strcpy(i8* %r5, i8* %str1)
    %r7 = call i8* @strcat(i8* %r6, i8* %str2)
    ret i8* %r7
}
