module Main where
import Frontend
import Backend
import System.IO
import System.Environment
import qualified Data.Set as S
import Control.Monad.Error
import Control.Monad.Reader
import Control.Monad.State
import Data.Array
import Data.Maybe
import LexLatte
import ParLatte 
import AbsLatte 
import ErrM
import System.Exit


exitAndShowError :: String -> IO ()
exitAndShowError errMessage = do
    hPutStrLn stderr "ERROR"
    hPutStrLn stderr errMessage
    exitFailure


runProgram :: String -> IO()
runProgram s=
	case pProgram $ myLexer s of
	Bad error -> exitAndShowError error
	Ok (Program prog) -> do
		validateProgram <- runErrorT(Frontend.compute prog)
		case validateProgram of	
			Left errMessage -> do

				 exitAndShowError errMessage
			Right _ -> do
				hPutStrLn stderr "OK"
				Backend.compute prog
		
main :: IO()
main = do
	name <- getProgName
	args <- getArgs
	case args of
		[fp] -> readFile fp >>= runProgram
		_ -> getContents >>= runProgram




