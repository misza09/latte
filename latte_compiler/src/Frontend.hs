module Frontend where

import AbsLatte hiding (Show)
import PrintLatte
import Control.Exception(catch)
import Utils (opimalizeExpression)
import Control.Monad.Reader
import Control.Monad.State
import Control.Monad.Error
import Data.List
import qualified Data.Map as Map
import qualified Data.Set as Set
import qualified Data.Vector as Vector

import Text.Printf


data Argument = Argument{
	aType :: Type,
	aName :: String
} deriving Show

data Register = Register{
	rType_ :: Type,
	rName :: String
} deriving Show

data Store = Store {
	functionsMap :: Map.Map String Type,
	variablesMap :: Map.Map String (Type, Integer),
	retInCurrentBlock :: Bool,
	expectedReturnType :: Type
} deriving Show

data TriValueLogic = TrueValue|FalseValue|NeutralValue

emptyStore :: Store
emptyStore = Store {
	functionsMap = Map.empty,
	variablesMap = Map.empty,
	retInCurrentBlock = False,
	expectedReturnType = Void
}

getTypeRep :: Type -> String
getTypeRep Int = "int"
getTypeRep Str = "string"
getTypeRep Bool = "boolean"
getTypeRep Void = "void"


type Computation = StateT Store (ErrorT String IO)

clearStore :: Computation()
clearStore = do
	store <- get
	put store{
		retInCurrentBlock = False,
		variablesMap = Map.empty
		}
	return ()

mainReturnTypeError :: Type -> Type -> String
mainReturnTypeError t1 t2  = "Wrong return type of \"main\" function: expected type " ++  getTypeRep t1 ++", actual type " ++  getTypeRep t2

expressionTypeError :: Print a => a -> Type -> Type -> String
expressionTypeError exp t1 t2 = "Wrong type of " ++ printTree exp ++ ", expected type " ++  getTypeRep t1 ++", actual type " ++  getTypeRep t2

checkFunctionParam :: (Expr, Type) -> Computation ()
checkFunctionParam (expr, t) = do
	expType <- checkExpression expr

	if expType == t then do
		return ()
	else do
		throwError ("Wrong parameter type. Expected " ++ getTypeRep t ++", but " ++ getTypeRep expType ++" has been applied" )
	


------------------------------Expressions
checkExpression :: Expr -> Computation Type
checkExpression (EVar (Ident ident)) = do
	store <- get
	let var = Map.lookup ident (variablesMap store)
	case var of	
		Nothing	-> throwError ("Variable " ++ ident ++ " is not declared")
		Just (t, depth) -> return t
	

checkExpression (ELitInt num) = do
	return Int

checkExpression (ELitTrue) = do
	return Bool

checkExpression (ELitFalse) = do
	return Bool

checkExpression fun@(EApp (Ident ident) exprs) = do
	store <- get
	let function = Map.lookup ident (functionsMap store)
	case function of
		Nothing -> throwError ("Error in expression: "++ (printTree fun) ++ " \n Function " ++ ident ++ " is not declared")
		Just (Fun t args) -> do
			if length args == length exprs then do
				mapM checkFunctionParam (zip exprs args)
				return t
			else 
				throwError (printf "Error in expression: %s \n Wrong number of parameter applied to function %s. Expected %s parameters, applied %s" (printTree fun) ident (show $ length exprs)(show $ length args))
			



checkExpression (EString str) = do
	return Str



checkExpression (Neg expr) = do
	expType <- checkExpression expr
	if expType == Int then do
		return Int
	else do
		throwError (expressionTypeError (Neg expr) Int expType)


checkExpression (Not expr) = do
	expType <- checkExpression expr
	if expType == Bool then do
		return Bool
	else do
		throwError (expressionTypeError (Not expr) Bool expType)



checkExpression (EMul exp1 op exp2) = do
	expType1 <- checkExpression exp1
	expType2 <- checkExpression exp2
	if expType1 == Int then do
		if expType2 == Int then do
			return Int
		else do
			throwError (expressionTypeError exp2 Int expType2)
	else do
		throwError (expressionTypeError exp2 Int expType1)


checkExpression (EAdd exp1 op exp2) = do
	expType1 <- checkExpression exp1
	expType2 <- checkExpression exp2
	case op of
		Plus ->
			if expType1 == Int then do
				if expType2 == Int then do
					return Int
				else do
					throwError (expressionTypeError exp2 Int expType2)
			else 
				if expType1 == Str then do
					if expType2 == Str then do
						return Str
					else do
						throwError (expressionTypeError exp2 Bool expType2)
				else throwError $ printf "Wrong return type of %s: expected type %s or %s, actual type %s"  (printTree exp1)(getTypeRep Int)(getTypeRep Str)(getTypeRep expType1)
		Minus ->
			if expType1 == Int then do
				if expType2 == Int then do
					return Int
				else do
					throwError (expressionTypeError exp2 Int expType2)
			else do
				throwError (expressionTypeError exp2 Int expType1)


checkExpression (ERel exp1 op exp2) = do
	expType1 <- checkExpression exp1
	expType2 <- checkExpression exp2
	if (expType1 == Void || expType2 == Void) then
		throwError "Wrong type. Cannot compare void type expression"
	else 
		if (expType1 == expType2) then
			return Bool
		else
			throwError (expressionTypeError exp2 expType1 expType2)
		



checkExpression (EAnd exp1 exp2) = do	
	expType1 <- checkExpression exp1
	expType2 <- checkExpression exp2
	case (expType1, expType2)  of
		(Bool, Bool) -> return Bool
		(_, Bool) -> throwError (expressionTypeError exp1 Bool expType1)
		(_, _) -> throwError (expressionTypeError exp2 Bool expType2)

checkExpression (EOr exp1 exp2) = do	
	expType1 <- checkExpression exp1
	expType2 <- checkExpression exp2
	case (expType1, expType2)  of
		(Bool, Bool) -> return Bool
		(_, Bool) -> throwError (expressionTypeError exp1 Bool expType1)
		(_, _) -> throwError (expressionTypeError exp2 Bool expType2)



-------------- /Expressions

checkIfCanDeclareVar :: String -> Integer -> Computation()
checkIfCanDeclareVar name depth = do
	store <- get
	let var = Map.lookup name (variablesMap store)
	case var of	
		Nothing	-> return ()
		Just (t, d) -> 
			if depth > d then do
				return ()
			else do
		 		throwError ("Variable " ++ name ++ " is already declared")


checkDeclCode :: Type -> Item -> Integer -> Computation ()
checkDeclCode t (NoInit (Ident n)) depth = do
	checkIfCanDeclareVar n depth
	storeVariable t n depth

	
checkDeclCode t (Init (Ident n) exp) depth = do
	expType <- checkExpression exp
	if expType == t then do
		return t
	else do
		throwError (expressionTypeError (Init (Ident n) exp) t expType)
	checkDeclCode t (NoInit (Ident n)) depth



checkDeclsCode :: Type ->[Item] -> Integer -> Computation()
checkDeclsCode t [] depth =
	return()
checkDeclsCode t (decl:decls) depth = do

  	(checkDeclCode t decl depth) `catchError` (\errorMessage -> throwError $ printf "error occured in declaration: " ++ printTree(decl) ++ " \n" ++ errorMessage)
	checkDeclsCode t decls depth




checkStmt :: Stmt -> Integer -> Computation()

checkStmt (Decl t decls) depth = do
	checkDeclsCode t decls depth 


checkStmt Empty depth =
	return ()

checkStmt st@(Ass (Ident ident) exp) depth= do
	expType <- checkExpression exp
	store <- get
	let var = Map.lookup ident (variablesMap store)
	case var of	
		Nothing	-> throwError ("Error in statement " ++ printTree st ++"Variable " ++ ident ++ " is not declared")
		Just (t, depth) -> do
			if expType == t then do
				storeVariable t ident depth
			 	return ()
			else
				throwError (expressionTypeError exp t expType)

checkStmt (Incr ident) depth = do
    checkStmt (Ass ident (EAdd (EVar ident) Plus (ELitInt 1))) depth >>	return()


checkStmt (Decr ident) depth = do
    checkStmt (Ass ident (EAdd (EVar ident) Minus (ELitInt 1))) depth >> return ()


checkStmt (Ret exp) depth = do
	expType <- checkExpression exp
	expectedType <- getExpectedRetType
	if expType == expectedType then do
		setRetInCurrentBlock True
	else
		throwError (expressionTypeError exp expectedType expType)
		
checkStmt (VRet) depth = do
	expectedType <- getExpectedRetType
	if Void == expectedType then do
		setRetInCurrentBlock True
	else
		throwError (expressionTypeError (VRet) expectedType Void)


	setRetInCurrentBlock True
	return ()

checkStmt (SExp exp) depth= do
	checkExpression exp
	return ()



checkStmt (BStmt (Block stmts)) depth = do
	store <- get
	let vMap = variablesMap store
	checkStmts stmts (depth+1)
	store <- get 
	put $ store{variablesMap = vMap}


checkStmt (Cond exp stmt) depth = do
	expType <- checkExpression exp
	let expVal = opimalizeExpression exp
	if expType == Bool then do
		checkStmt stmt (depth+1)
		case expVal of
			(ELitTrue) -> return ()
			_ -> do
				setRetInCurrentBlock False
				return ()
	else
		throwError (expressionTypeError exp Bool expType)

checkStmt (CondElse exp stmt1 stmt2) depth = do
	expType <- checkExpression exp
	let expVal = opimalizeExpression exp
	if expType == Bool then do
		checkStmt stmt1 (depth+1)
		ret1 <-	getRetInCurrentBlock
		setRetInCurrentBlock False 
		checkStmt stmt2 (depth+1)
		ret2 <-	getRetInCurrentBlock
		case expVal of
			(ELitTrue) -> setRetInCurrentBlock ret1
			(ELitFalse) -> setRetInCurrentBlock ret2
			_ -> do
				setRetInCurrentBlock (ret1 && ret2)
		return ()
	else
		throwError (expressionTypeError exp Bool expType)


checkStmt (While exp stmt) depth = do
	expType <- checkExpression exp
	ret <-	getRetInCurrentBlock

	if expType == Bool then do
		checkStmt stmt (depth+1)
		let expVal = opimalizeExpression exp
		if	expVal == ELitFalse then do
			setRetInCurrentBlock ret
		else
			return ()
			 
	else
		throwError (expressionTypeError exp Bool expType)
 

checkStmts :: [Stmt] -> Integer -> Computation()
checkStmts [] t =
	return ()
checkStmts (stmt:stmts) d = do
	checkStmt stmt d
	checkStmts stmts d


checkBlock :: Block -> Computation()
checkBlock (Block stmts) =
	checkStmts stmts 1



setExpectedRetType :: Type ->Computation ()
setExpectedRetType t = do
	store <- get
	put $ store{expectedReturnType = t}
	return ()

getExpectedRetType :: Computation Type
getExpectedRetType = do
	store <- get
	return (expectedReturnType store)


setRetInCurrentBlock :: Bool ->Computation ()
setRetInCurrentBlock t = do
	store <- get
	put $ store{retInCurrentBlock = t}
	return ()

getRetInCurrentBlock :: Computation Bool
getRetInCurrentBlock = do
	store <- get
	return (retInCurrentBlock store)


storeVariable :: Type -> String -> Integer -> Computation ()
storeVariable varType varName depth = do
	store <- get
	put $ store{variablesMap = (Map.insert varName (varType, depth) (variablesMap store))}
	return ()

storeArgument :: Arg -> Computation()
storeArgument  (Arg t (Ident n)) = do
	storeVariable t n 0
	return ()

checkReturn :: Type -> Computation()
checkReturn t = do
	case t of
		Void -> return ()
 		_ -> do
			ret <- getRetInCurrentBlock
			if ret then	return ()
			else throwError "No return statement in function block."	


checkFunction :: TopDef -> Computation()
checkFunction (FnDef t (Ident name) args block) = do
	clearStore
	setExpectedRetType t
	mapM storeArgument args
	checkBlock block
	checkReturn t
	return ()

checkTopDef :: TopDef -> Computation()
checkTopDef fun@(FnDef t (Ident name) args block) = do
	(checkFunction fun) `catchError` (\errorMessage -> throwError $ printf "error occured in function: " ++ name ++ " \n" ++ errorMessage)
	

checkMainFunction :: Computation ()
checkMainFunction = do
    store <- get
    case Map.lookup "main" (functionsMap store) of		
        Just (Fun Int []) -> return ()
        Just (Fun Int _) ->  throwError "function \"int main()\" should not take any arguments"
        Just (Fun t _) ->  throwError  (mainReturnTypeError Int t)
        Nothing -> throwError "function \"int main()\" is not declared"




declareFunctionTopDef :: TopDef -> Computation()
declareFunctionTopDef (FnDef t (Ident name)  args _) = do

	let arguments = map (\(Arg t (Ident n)) -> t) args
	let argumentsNames = map (\(Arg t (Ident n)) -> n) args
	if length (nub argumentsNames) == length args then do
		declareFunction  t name arguments >>= return
	else 
		throwError $ "Dupicated parameters names in function " ++ name

declareFunction :: Type -> String -> [Type] -> Computation()
declareFunction t name arguments = do
	store <- get
	case Map.lookup name (functionsMap store) of
		Just _ -> throwError $ "Dupicated function body: " ++ name
		Nothing -> do
			put $ store{functionsMap = (Map.insert name (Fun t arguments) (functionsMap store))}
			return ()


declareFunctions :: [TopDef] -> Computation()
declareFunctions (a:l)  = do
	result <- declareFunctionTopDef a
	declareFunctions l
declareFunctions []  = do
	return ()

declareBuildInFunctions :: Computation()
declareBuildInFunctions = do
	declareFunction Void "printInt" [Int]
	declareFunction Void "printString" [Str]
	declareFunction Void "error" []
	declareFunction Int "readInt" []
	declareFunction Str "readString" []


checkProgram :: [TopDef] -> Computation ()
checkProgram funs = do
	declareBuildInFunctions
	declareFunctions funs
	checkMainFunction
	mapM checkTopDef funs
	store <-get
	return ()


compute :: [TopDef] -> (ErrorT String IO)()
compute funs  = 
	evalStateT (checkProgram funs) (emptyStore)

