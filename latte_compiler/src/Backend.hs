module Backend where

import AbsLatte hiding (Show)
import Utils (opimalizeExpression)
import Control.Monad.Reader
import Control.Monad.State
import Data.List
import qualified Data.Map as Map
import qualified Data.Set as Set
import qualified Data.Vector as Vector

import Text.Printf

data Argument = Argument{
	aType :: Type,
	aName :: String
} deriving Show

data Register = Register{
	rType_ :: Type,
	rName :: String
} deriving Show

data FunctionDecl = FunctionDecl {
	fType :: Type,
	fName :: String,
	fArgs :: [Type]
} deriving Show


data Store = Store {
    regCounter :: Integer,
    labelCounter :: Integer,
	currentLabel :: String,
	functionsMap :: Map.Map String FunctionDecl,
	variablesMap :: Map.Map String Register,
	stringsMap :: Map.Map String String,
    stringCounter :: Integer,
	retInCurrentBlock :: Bool,
    codeLLVM :: [String]
} deriving Show


emptyStore :: Store
emptyStore = Store {
    regCounter = 0,
    labelCounter = 0,
	currentLabel = "0",
	functionsMap = Map.empty,
	variablesMap = Map.empty,
	stringsMap = Map.empty,
	retInCurrentBlock = False,
    stringCounter = 0,
  	codeLLVM = []
}


getLLVMType :: Type -> String

getLLVMType Int = "i32"
getLLVMType Bool = "i1"
getLLVMType Str = "i8*"
getLLVMType Void = "void"


getLLVMMulOp Times = "mul"
getLLVMMulOp Div = "sdiv"
getLLVMMulOp Mod = "srem"
getLLVMAddOp Plus = "add"
getLLVMAddOp Minus = "sub"
getLLVMRelOp LTH = "slt"
getLLVMRelOp LE = "sle"
getLLVMRelOp GTH = "sgt"
getLLVMRelOp GE = "sge"
getLLVMRelOp EQU = "eq"
getLLVMRelOp NE = "ne"


defaultValue :: Type -> Expr
defaultValue Int = (ELitInt 0)
defaultValue Bool = (ELitFalse)
defaultValue Str = EString ""



type Computation = StateT Store IO

clearStore :: Computation()
clearStore = do
	store <- get
	put store{
		regCounter = 0,
		labelCounter = 0,
		currentLabel = "0",
		retInCurrentBlock = False,
		variablesMap = Map.empty
		}
	return ()

setRetForBlock :: Bool -> Computation()
setRetForBlock val = do
	store <- get
	put $ store{retInCurrentBlock = val}

getRetForBlock :: Computation Bool
getRetForBlock = do
	store <- get
	return $ retInCurrentBlock store


pushCodeLLVM :: [String] -> Computation ()
pushCodeLLVM s = do
	store <- get
	let prevCodeLLVM = codeLLVM store 	
	put $ store{codeLLVM = (s ++ prevCodeLLVM) }
	return ()


pushCodeLineLLVM :: String -> Computation ()
pushCodeLineLLVM s = do
	pushCodeLLVM [s]
	return ()

pushCodeLineLLVMOnFront s = do
	store <- get
	let prevCodeLLVM = codeLLVM store 	
	put $ store{codeLLVM = (prevCodeLLVM ++ [s]) }
	return ()

getNextRegister :: Computation String
getNextRegister = do
	store <- get
	let reg = regCounter store
	put $ store {regCounter = reg + 1 }
	return ("r" ++ show (reg + 1))

getNextLabel :: Computation String
getNextLabel = do
	store <- get
	let label = labelCounter store
	put $ store {labelCounter = label + 1 }
	return $ printf "label%s"  (show(label +1))



getRegister :: Type -> String -> Register
getRegister t name =
 Register{rType_ = t, rName = "%" ++ name}

getValRegister :: Type -> String -> Register
getValRegister t name =
 Register{rType_ = t, rName = name}


showListWithSeparator :: [String] -> String
showListWithSeparator list =
	intercalate "," list


getStringRef :: String -> Computation String
getStringRef str = do
	store <- get
	let ref =  Map.lookup str (stringsMap store)
	case ref of		
		Just r -> return $ (r)
		Nothing -> do
			let num = stringCounter store
			let ref = "@.str" ++ show num
			let sMap = Map.insert str ref (stringsMap store)
			put $ store {stringCounter = num + 1, stringsMap = sMap}
			return $ (ref)


showArgument :: Arg -> String
showArgument (Arg t (Ident name)) =
	printf "%s %%%s" (getLLVMType t) name 

showArguments :: [Arg] -> String
showArguments args = 
	showListWithSeparator $ map showArgument args


loadRegister :: Register -> Computation (Register, [String])
loadRegister register = do
	regId <- getNextRegister
	let newRegister = getRegister (rType_ register) (regId)
	let loadCode = printf "  %s = load %s, %s* %s" (rName newRegister) (getLLVMType $ rType_ register) (getLLVMType $ rType_ register) (rName register)	
	return $ (newRegister, [loadCode])

paramCode :: (Type, Register) -> String
paramCode (t,val) = printf "%s %s" (getLLVMType t) (rName val)


------------------------------Expression
computeExpression :: Expr -> Computation(Register, [String])
computeExpression (EVar (Ident ident)) = do
	store <- get
	let Just register = Map.lookup ident (variablesMap store)
	(expRegister, expCode) <- loadRegister register
 	return (expRegister, expCode)

computeExpression (ELitInt num) = do
	let register = getValRegister Int (show $ num)
	return (register, [])

computeExpression (ELitTrue) = do
	let register = getValRegister Bool "true"
	return (register, [])

computeExpression (ELitFalse) = do
	let register = getValRegister Bool "false"
	return (register, [])

computeExpression (EApp (Ident ident) exprs) = do
	store <- get
	let Just function = Map.lookup ident (functionsMap store)

	x <-(mapM (\expr -> computeExpression expr >>= return) exprs)
	let (paramValues, exprCodeArray)  = unzip x
	let exprCode = foldl (++) [] exprCodeArray
	let paramMap = map paramCode (zip (fArgs function) paramValues)
	let paramsCode = showListWithSeparator paramMap

	let result = printf "call %s @%s (%s)" (getLLVMType $ fType function) ident paramsCode

 	case (fType function) of
		Void -> do
			let register = getRegister Void ""
			return (register, ["  " ++ result] ++ exprCode)
		_-> do
			regId <- getNextRegister
			let newRegister = getRegister (fType function) (regId)
			let assResult = (printf "  %s = " (rName newRegister)) ++ result
			return $ (newRegister,   [assResult] ++ exprCode)


computeExpression (EString str) = do
	ref <- getStringRef str
	regId <- getNextRegister
	let newRegister = getRegister Str (regId)
	let result = printf "  %s = getelementptr inbounds [%s x i8], [%s x i8]* %s, i32 0, i32 0" (rName newRegister) (show ((length str)+1)) (show ((length str)+1)) ref
	return (newRegister, [result])

computeExpression (Neg expr) = do
	computeExpression (EAdd (ELitInt 0) Minus expr) >>= return


computeExpression (Not expr) = do
	(register, exprCode) <- computeExpression expr
	regId <- getNextRegister
	let newRegister = getRegister Bool (regId)
	let result = printf ("  %s = xor %s %s, true") (rName newRegister) (getLLVMType Bool) (rName register)::String
	return (newRegister, [result] ++ exprCode)

computeExpression (EMul exp1 op exp2) = do
	(register1, exprCode1) <- computeExpression exp1
	(register2, exprCode2) <- computeExpression exp2
	regId <- getNextRegister
	let newRegister = getRegister Int (regId)
	let result = printf ("  %s = %s %s %s, %s") (rName newRegister) (getLLVMMulOp op) (getLLVMType Int) (rName register1) (rName register2)
	return (newRegister, [result] ++ exprCode2 ++ exprCode1)

computeExpression (EAdd exp1 op exp2) = do
	(register1, exprCode1) <- computeExpression exp1
	(register2, exprCode2) <- computeExpression exp2
	case (rType_ register1) of 
		Str -> do
			regId <- getNextRegister
			let newRegister = getRegister Str (regId)
			let result = printf "  %s = call i8* @concat_(i8* %s , i8* %s)"  (rName newRegister) (rName register1) (rName register2)
			return (newRegister, [result] ++ exprCode2 ++ exprCode1)	
		_-> do
			regId <- getNextRegister
			let newRegister = getRegister Int (regId)
			let result = printf ("  %s = %s %s %s, %s") (rName newRegister) (getLLVMAddOp op) (getLLVMType Int) (rName register1) (rName register2)
			return (newRegister, [result] ++ exprCode2 ++ exprCode1)

		
computeExpression (ERel exp1 op exp2) = do
	(register1, exprCode1) <- computeExpression exp1
	(register2, exprCode2) <- computeExpression exp2
	regId <- getNextRegister
	let newRegister = getRegister Bool (regId)
	let result = printf ("  %s = icmp %s %s %s, %s") (rName newRegister) (getLLVMRelOp op) (getLLVMType (rType_ register1)) (rName register1) (rName register2)
	return (newRegister, [result] ++ exprCode2 ++ exprCode1)


computeExpression (EAnd exp1 exp2) = do	

	(register1, exprCode1) <- computeExpression exp1
	trueLabel <- getNextLabel
	let trueLabelStr = printf "%s:" trueLabel

	store <- get
	let firstLabel = currentLabel store
	put store{currentLabel = trueLabel}
	(register2, exprCode2) <- computeExpression exp2

	finishLabel <- getNextLabel
	let finishLabelStr = printf "%s:" finishLabel


	let result1 =  "  br i1 " ++ (rName register1) ++", label %" ++ trueLabel ++ ", label %" ++  finishLabel
	let result2 = "  br label %" ++ finishLabel

	store <- get
	let secondLabel = currentLabel store
	put store{currentLabel = finishLabel}	

	regId <- getNextRegister
	let newRegister = getRegister Bool (regId)
	let result = printf "  %s = phi i1 [ false, %%%s ] , [ %s, %%%s ]"  (rName newRegister) (firstLabel) (rName register2) secondLabel



	return (newRegister, [result] ++ [finishLabelStr, ""] ++ [result2] ++ exprCode2 ++ [trueLabelStr, ""] ++ [result1] ++ exprCode1)

computeExpression (EOr exp1 exp2) = do	

	(register1, exprCode1) <- computeExpression exp1
	falseLabel <- getNextLabel
	let falseLabelStr = printf "%s:" falseLabel

	store <- get
	let firstLabel = currentLabel store
	put store{currentLabel = falseLabel}
	(register2, exprCode2) <- computeExpression exp2

	finishLabel <- getNextLabel
	let finishLabelStr = printf "%s:" finishLabel
 
	let result1 =  "  br i1 " ++ (rName register1) ++", label %" ++ finishLabel ++ ", label %" ++  falseLabel
	let result2 =  "  br label %" ++ finishLabel
	
	store <- get
	let secondLabel = currentLabel store
	put store{currentLabel = finishLabel}

	regId <- getNextRegister
	let newRegister = getRegister Bool (regId)
	let result = printf "  %s = phi i1 [ true, %%%s ] , [ %s, %%%s ]"  (rName newRegister) firstLabel  (rName register2) secondLabel
	
	return $ (newRegister, [result] ++ [finishLabelStr, ""] ++ [result2] ++ exprCode2 ++ [falseLabelStr, ""] ++ [result1] ++ exprCode1)



--------------------
computeCondExpression :: Expr -> String -> String -> Computation(String,[String])

computeCondExpression (EAnd exp1 exp2) trueLabel falseLabel = do

	middleLabel <- getNextLabel
	let middleLabelStr = printf "%s:" middleLabel
	(register1, exprCode1) <- computeCondExpression exp1 middleLabel falseLabel
	store <- get
	let firstLabel = currentLabel store
	put store{currentLabel = middleLabel}	

	(register2, exprCode2) <- computeCondExpression exp2 trueLabel falseLabel
	
	store <- get
	put store{currentLabel = trueLabel}	
	return ("", exprCode2 ++ [middleLabelStr] ++ exprCode1)


computeCondExpression (EOr exp1 exp2) trueLabel falseLabel = do
	middleLabel <- getNextLabel
	let middleLabelStr = printf "%s:" middleLabel
	(register1, exprCode1) <- computeCondExpression exp1 trueLabel middleLabel
	store <- get
	put store{currentLabel = middleLabel}	

	(register2, exprCode2) <- computeCondExpression exp2 trueLabel falseLabel
	
	store <- get
	put store{currentLabel = trueLabel}	
	return ("", exprCode2 ++ [middleLabelStr] ++ exprCode1)

computeCondExpression (Not exp) trueLabel falseLabel = do
	computeCondExpression exp falseLabel trueLabel >>= return

computeCondExpression exp trueLabel falseLabel = do
	(register, exprCode) <-  computeExpression exp
	let result =  "  br i1 " ++ (rName register) ++", label %" ++ trueLabel ++ ", label %" ++  falseLabel
	return ("", result:exprCode)



storeVarRegister :: String -> Register -> Computation()
storeVarRegister name register = do
	store <- get
	put $ store{variablesMap = (Map.insert name register (variablesMap store))}
	return ()

allocRegister :: Register -> Computation String
allocRegister register = do
	return $ printf "  %s = alloca %s" (rName register)(getLLVMType $ rType_ register)

allocVariable :: Type -> String -> Computation (Register, String)
allocVariable varType varName = do
	regId <- getNextRegister
	let register = getRegister varType regId
	storeVarRegister varName register
	allocCode <- allocRegister register
	return $ (register, allocCode)


storeVal :: Register -> String -> Computation String
storeVal register argName = do
	store <- get
	let regType = getLLVMType $ rType_ register
	return $ (printf "  store %s %s, %s* %s" regType argName regType (rName register))


allocArgument :: Type -> String ->Computation (Register, String)
allocArgument varType varName = do
	regId <- getNextRegister
	let register = getRegister varType (regId)
	storeVarRegister varName register
	allocCode <- allocRegister register
	return $ (register, allocCode)


prepareArgument :: Arg -> Computation [String]
prepareArgument (Arg t (Ident n)) = do
	(register, allocCode) <- allocArgument t n
	storeCode <- storeVal register ("%" ++ n)
	return $ ([storeCode] ++ [allocCode])


prepareArguments :: [Arg] -> Computation [String]
prepareArguments (h:t) = do
	hCode<-prepareArgument h
	tCode<-prepareArguments t
	return (tCode ++ hCode)
prepareArguments [] = return []


generateStmtCode :: Stmt -> Computation [String]
generateStmtCode (Decl t decls) = do
	code <- generateDeclsCode t decls
	return code

generateStmtCode (Ass (Ident ident) exp) = do
	let oexp = opimalizeExpression exp
	(register, exprCode) <- computeExpression oexp
	store <- get
	let Just oldRegister = Map.lookup ident (variablesMap store)
	storeCode <- storeVal oldRegister (rName register)
	return $ (storeCode:exprCode)

generateStmtCode (Incr ident) = do
    generateStmtCode (Ass ident (EAdd (EVar ident) Plus (ELitInt 1))) >>= return

generateStmtCode (Decr ident) = do
    generateStmtCode (Ass ident (EAdd (EVar ident) Minus (ELitInt 1))) >>= return

generateStmtCode (Ret exp) = do
	let oexp = opimalizeExpression exp
	(register, exprCode) <- computeExpression oexp
	setRetForBlock True
	return $ (("  ret " ++ (getLLVMType $ rType_ register) ++ " " ++ (rName register)):exprCode)

generateStmtCode (VRet) = do
	setRetForBlock True
	return $ ["  ret void"]

generateStmtCode (SExp exp) = do
	let oexp = opimalizeExpression exp
	(value, exprCode) <- computeExpression oexp
	return $ exprCode

generateStmtCode (Cond exp stmt) = do
	let oexp = opimalizeExpression exp
	case oexp of
		ELitTrue -> (generateStmtCode stmt) >>= return
		ELitFalse -> return $ ([])
		_-> do
			trueLabel <- getNextLabel
			let trueLabelStr = printf "%s:" trueLabel
			falseLabel <- getNextLabel
			let falseLabelStr = printf "%s:"  falseLabel
			(expValue, expCode) <- computeCondExpression oexp trueLabel falseLabel

			store <- get
			let vMap = variablesMap store
			stmtCode <- generateStmtCode stmt
			store <- get 
			put $ store{variablesMap = vMap}

			let ret =  "  br label %" ++ falseLabel
			store <- get
			put store{currentLabel = falseLabel}

			return $ ([falseLabelStr] ++ ret:stmtCode ++ [trueLabelStr] ++ expCode)

generateStmtCode (CondElse exp stmt1 stmt2) = do
	let optExp = opimalizeExpression exp
	case optExp of
		ELitTrue -> (generateStmtCode stmt1) >>= return
		ELitFalse -> (generateStmtCode stmt2) >>= return
		_-> do
			trueLabel <- getNextLabel
			let trueLabelStr = printf "%s:"  trueLabel
			falseLabel <- getNextLabel
			let falseLabelStr = printf "%s:"  falseLabel
			(expValue, expCode) <- computeCondExpression optExp trueLabel falseLabel

			store <- get
			let vMap1 = variablesMap store
			stmtCode1 <- generateStmtCode stmt1
			store <- get 
			put $ store{variablesMap = vMap1}

			ret1 <- getRetForBlock
			setRetForBlock False

			store <- get
			let vMap2 = variablesMap store
			stmtCode2 <- generateStmtCode stmt2
			store <- get 
			put $ store{variablesMap = vMap2}

			ret2 <- getRetForBlock
			setRetForBlock False
			afterLabel <- getNextLabel

			if ret1 && ret1 then do
				setRetForBlock True
				return $ (stmtCode2 ++[falseLabelStr] ++ stmtCode1 ++ [trueLabelStr] ++ expCode)
			else do	
				let afterLabelStr = printf "%s:"  afterLabel
				let afterStmtJump = "  br label %" ++ afterLabel
				store <- get
				put store{currentLabel = afterLabel}
				return $ ([afterLabelStr] ++ afterStmtJump:stmtCode2 ++[falseLabelStr] ++ afterStmtJump:stmtCode1 ++ [trueLabelStr] ++ expCode)


generateStmtCode (While exp stmt) = do
	let optExp = opimalizeExpression exp
	case optExp of
	 	ELitFalse -> return $ ([])
		_-> do
			expLabel <- getNextLabel
			let expLabelStr = expLabel ++ ":"
			trueLabel <- getNextLabel
			let trueLabelStr = trueLabel ++ ":"
			falseLabel <- getNextLabel
			let falseLabelStr = falseLabel ++ ":"
			(expValue, expCode) <- computeCondExpression optExp trueLabel falseLabel

			store <- get
			let vMap = variablesMap store
			stmtCode <- generateStmtCode stmt
			store <- get 
			put $ store{variablesMap = vMap}

			let afterStmtJump = "  br label %" ++ expLabel

			return $ ([falseLabelStr] ++ afterStmtJump:stmtCode ++ [trueLabelStr] ++ expCode ++ [expLabelStr, afterStmtJump])



generateStmtCode (BStmt (Block stmts)) = do
	store <- get
	let vMap = variablesMap store
	stmtsCode <- generateBlockCode stmts
	store <- get 
	put $ store{variablesMap = vMap}
	return $ stmtsCode


generateStmtCode _ = do
	return []

generateDeclsCode :: Type -> [Item] -> Computation [String]
generateDeclsCode t (h:tail)= do
	hCode<-generateDeclCode t h
	tCode<-generateDeclsCode t tail
	return (tCode ++ hCode)
generateDeclsCode _ [] = return []



generateDeclCode :: Type -> Item -> Computation [String]
generateDeclCode t (NoInit (Ident n))= do
	generateDeclCode t (Init (Ident n) (defaultValue t)) >>= return

generateDeclCode t (Init (Ident n) exp )= do
	let optExp = opimalizeExpression exp
	(expRegister, expCode) <- computeExpression optExp
	(register, allocCode) <- allocVariable t n
	storeCode <- storeVal register (rName expRegister)
	return $ ([storeCode] ++ [allocCode] ++ expCode)	






generateBlockCode :: [Stmt] -> Computation [String]
generateBlockCode (stmt:stmts) = do
	hCode<-generateStmtCode stmt
	tCode<-generateBlockCode stmts
	return (tCode ++ hCode)
generateBlockCode [] = do
	return []




computeFunction :: TopDef -> Computation()
computeFunction (FnDef t (Ident name)  args (Block block)) = do
	clearStore
	pushCodeLineLLVM  (printf "define %s @%s(%s) #0 {" (getLLVMType t) name (showArguments args ) )
	argsCode <-  (prepareArguments args)
	pushCodeLLVM argsCode
	blockCode <- generateBlockCode block

	pushCodeLLVM blockCode
	hasRet <- getRetForBlock
	if ((not hasRet) && t == Void) then do
		retCode <- generateStmtCode (VRet) 
		pushCodeLLVM retCode
	else
		return()
		
	pushCodeLineLLVM  "}\n"
	return ()

addBuildInStringDeclaration :: Computation()
addBuildInStringDeclaration = do
	pushCodeLineLLVM "declare void @printInt(i32)"
	pushCodeLineLLVM "declare void @printString(i8*)"
	pushCodeLineLLVM "declare void @error()"
	pushCodeLineLLVM "declare i32 @readInt()"
	pushCodeLineLLVM "declare i8* @readString()"
	pushCodeLineLLVM "declare i8* @concat_(i8* , i8*)"
	pushCodeLineLLVM "declare i8* @malloc(i32)"
	pushCodeLineLLVM ""
	declareFunction Void "printInt" [Int]
	declareFunction Void "printString" [Str]
	declareFunction Void "error" []
	declareFunction Int "readInt" []
	declareFunction Str "readString" []



generateCodeStrings :: Computation ()
generateCodeStrings = do
		store <- get
		pushCodeLineLLVMOnFront("")
		let pushString (str, label) = do
			pushCodeLineLLVMOnFront  $ printf "%s = private constant [%s x i8] c\"%s\\00\"" label (show (length str + 1)) str
		mapM_ pushString (reverse (Map.toList (stringsMap store))) 

		return ()


computeFunctions :: [TopDef] -> Computation()
computeFunctions []  = do
	return ()

computeFunctions (a:l)  = do
  result <- computeFunction a
  computeFunctions l 



declareFunctionTopDef :: TopDef -> Computation()
declareFunctionTopDef (FnDef t (Ident name)  args _) = do
	let arguments = map (\(Arg t (Ident n)) -> t) args
	declareFunction  t name arguments >>= return

declareFunction :: Type -> String -> [Type] -> Computation()
declareFunction t name arguments = do
	let function = FunctionDecl {fName= name, fType = t, fArgs = arguments}
	store <- get
	put $ store{functionsMap = (Map.insert name function (functionsMap store))}
	return ()


declareFunctions :: [TopDef] -> Computation()
declareFunctions (a:l)  = do
	result <- declareFunctionTopDef a
	declareFunctions l
declareFunctions []  = do
	return ()



declareAndComputeFunctions :: [TopDef] -> Computation()
declareAndComputeFunctions funs = do
	addBuildInStringDeclaration
	declareFunctions funs
	computeFunctions funs
	generateCodeStrings
	store <-get
	liftIO $ putStr $ intercalate "\n" (reverse $ codeLLVM store)

compute :: [TopDef] -> IO()
compute funs  = do

	evalStateT (declareAndComputeFunctions funs) (emptyStore)
