module Utils where
import AbsLatte hiding (Show)

getMulOp :: MulOp -> Integer -> Integer -> Integer 
getMulOp Times = (*)
getMulOp Div = div
getMulOp Mod = mod
getAddOp :: AddOp -> Integer -> Integer -> Integer 
getAddOp Plus = (+)
getAddOp Minus = (-)
getBoolOp :: Ord a => RelOp -> a -> a -> Bool 
getBoolOp LTH = (<)
getBoolOp LE = (<=)
getBoolOp GTH = (>)
getBoolOp GE = (>=)
getBoolOp EQU = (==)
getBoolOp NE = (/=)

boolToELitBool True = ELitTrue
boolToELitBool False = ELitFalse

opimalizeExpression  :: Expr ->Expr
opimalizeExpression (EVar (Ident ident)) =
	 (EVar (Ident ident))
opimalizeExpression (ELitInt num) =
	 (ELitInt num)
opimalizeExpression (ELitTrue) =
	 (ELitTrue)
opimalizeExpression (ELitFalse) =
	 (ELitFalse)
opimalizeExpression (EApp (Ident ident) exprs) =
	let optExpr = map opimalizeExpression exprs in
	EApp (Ident ident) optExpr
opimalizeExpression (EString str)=
	(EString str)
opimalizeExpression (Neg expr) =
	let optExp = opimalizeExpression expr in
	case optExp of
		 (ELitInt num) -> (ELitInt (-num))
		 _-> (Neg optExp)



opimalizeExpression (Not expr) = 
	let optExp = opimalizeExpression expr in
	case optExp of
		ELitTrue -> ELitFalse
		ELitFalse -> ELitTrue
		_ -> (Not optExp)
opimalizeExpression (EMul exp1 op exp2) =
	let optExp1 = opimalizeExpression exp1 in
	let optExp2 = opimalizeExpression exp2 in
	case (optExp1, optExp2) of
		(ELitInt num1, ELitInt num2) -> ELitInt((getMulOp op) num1 num2) 
		(_, _) ->  (EMul optExp1 op optExp2)

opimalizeExpression (EAdd exp1 op exp2) =
	let optExp1 = opimalizeExpression exp1 in
	let optExp2 = opimalizeExpression exp2 in
	case (optExp1, optExp2) of
		(ELitInt num1, ELitInt num2) -> ELitInt((getAddOp op) num1 num2)
		(_, _) -> (EAdd optExp1 op optExp2)

opimalizeExpression (ERel exp1 op exp2) =
	let optExp1 = opimalizeExpression exp1 in
	let optExp2 = opimalizeExpression exp2 in
	case (optExp1, optExp2) of
		(ELitInt num1, ELitInt num2) -> 
			boolToELitBool ((getBoolOp op) num1 num2)
		(EString s1, EString s2) ->
			boolToELitBool ((getBoolOp op) s1 s2)
		(ELitTrue, ELitTrue) ->
			boolToELitBool ((getBoolOp op) True True)
		(ELitFalse, ELitFalse) ->
			boolToELitBool ((getBoolOp op) False False)
		(ELitTrue, ELitFalse) ->
			boolToELitBool ((getBoolOp op) True False)
		(ELitFalse, ELitTrue) ->
			boolToELitBool ((getBoolOp op) False True)
		(_, _) -> 
			 (ERel optExp1 op optExp2)
				
		
opimalizeExpression (EAnd exp1 exp2) =
	let optExp1 = opimalizeExpression exp1 in
	let optExp2 = opimalizeExpression exp2 in
	case (optExp1, optExp2) of
		(ELitTrue, ELitTrue) -> ELitTrue
		(ELitFalse, _) -> ELitFalse
		(_, ELitFalse) -> ELitFalse
		(ELitTrue, _) -> optExp2
		(_, ELitTrue) -> optExp1
		(_, _) -> (EAnd optExp1 optExp2)

opimalizeExpression (EOr exp1 exp2) =
	let optExp1 = opimalizeExpression exp1 in
	let optExp2 = opimalizeExpression exp2 in
	case (optExp1, optExp2) of
		(ELitFalse, ELitFalse) -> ELitFalse
		(ELitTrue, _) -> ELitTrue
		(_, ELitTrue) -> ELitTrue
		(ELitFalse, _) -> optExp2
		(_, ELitFalse) -> optExp1
		(_, _) -> (EOr optExp1 optExp2)
	
